package com.nobi.pacman.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.nobi.pacman.Main;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

        
        TexturePacker.Settings s = new TexturePacker.Settings();
        s.filterMag = Texture.TextureFilter.Linear;
        s.filterMin = Texture.TextureFilter.Linear;
        s.premultiplyAlpha = false;
        s.debug = true;
        TexturePacker.process(s,"D:\\Android\\Projects\\Pacman\\pacman\\OtherFiles\\atlas", "D:\\Android\\Projects\\Pacman\\pacman\\android\\assets", "atlas");



        switch(    1    ){
			
			
			case 1 : 
				config.height = 320;
				config.width = 480;
				break;
			case 2 : 
				config.height = 480;
				config.width = 800;
				break;
			case 3 : 
				config.height = 720;
				config.width = 1280;
				break;
			case 4 : 
				config.height = 1080;
				config.width = 1920;
				
				break;
		}
		
		
		config.resizable = false;
		new LwjglApplication(new Main(), config);
	}
}
