package com.nobi.pacman;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;

public class WorldContactListener implements ContactListener{

    Fixture fixA,fixB;
    Main main;
    
    int sensor_coll_count = 0;
    
    public WorldContactListener(Main m){
        main = m;
    }
    
    private boolean overlap(String A, String B){


        B2DUserdata fiA = (B2DUserdata) fixA.getUserData();
        B2DUserdata fiB = (B2DUserdata) fixB.getUserData();
        
        if( (fiA.name.equals(A) && fiB.name.equals(B))   || (fiA.name.equals(B) && fiB.name.equals(A)) )
            return true;
        return false;
    }

    private boolean singleoverlap(B2DUserdata A, B2DUserdata B, String C){
        if(A.name.equals(C) || B.name.equals(C)) return true;
        return false;
    }
    
    @Override
    public void beginContact(Contact contact){
        fixA = contact.getFixtureA();
        fixB = contact.getFixtureB();
        
        B2DUserdata A = (B2DUserdata) fixA.getUserData();
        B2DUserdata B = (B2DUserdata) fixB.getUserData();
        
        log.y("BEGIN"+" "+A.name+" "+B.name);
        
        if(singleoverlap(A,B,"p_sensor")){
            B2DUserdata C  = A.name.equals("p_sensor") ? A : B;
            ((Player)C.object).canJump=true;
            log.c("canJump");
            sensor_coll_count+=1;
        }


    }

    @Override
    public void endContact(Contact contact){
        fixA = contact.getFixtureA();
        fixB = contact.getFixtureB();

        B2DUserdata A = (B2DUserdata) fixA.getUserData();
        B2DUserdata B = (B2DUserdata) fixB.getUserData();

        log.y("END"+" "+A.name+" "+B.name);
        
        if(singleoverlap(A,B,"p_sensor")){
            B2DUserdata C  = A.name.equals("p_sensor") ? A : B;
            sensor_coll_count-=1;
            if(sensor_coll_count==0){
                ((Player) C.object).canJump = false;
                log.c("notcanJump");
            }
        }

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold){

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse){

    }
}
