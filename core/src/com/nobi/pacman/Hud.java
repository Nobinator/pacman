package com.nobi.pacman;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class Hud{
    public Stage stage;
    private Viewport viewport;
    
    Label simplelabel;
    GameScreen screen;
    
    public Hud(SpriteBatch sb,Screen screen){
        this.screen = (GameScreen) screen;
        viewport = new FitViewport(Gdx.graphics.getWidth(),Gdx.graphics.getHeight(), new OrthographicCamera());
        stage = new Stage(viewport,sb);
        
        Table table = new Table();
        table.top();
        table.setFillParent(true);

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("font.otf"));
        FreeTypeFontGenerator.FreeTypeFontParameter param = new FreeTypeFontGenerator.FreeTypeFontParameter();
        param.size = 20;
        param.color = Color.valueOf("494949");
        BitmapFont FONT = generator.generateFont(param);
        generator.dispose();
        
        simplelabel = new Label("text", new Label.LabelStyle(FONT,param.color));
        
        table.add(simplelabel);
        stage.addActor(table);
                
    }
    
    public void update(float dt){
        simplelabel.setText(screen.p.currentstate+" ");
        stage.act(dt);
    }
    
    
    public void dispose(){
        stage.dispose();
    }
}
