package com.nobi.pacman;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;

public abstract class Enemy extends Sprite {
    
    World world;
    TiledMap map;
    public Body body;
    
    public Enemy(World w,TiledMap m, float x, float y){
        world = w;
        map = m;
        setPosition(x,y);
        defineEnemy();
    }
    
    abstract void defineEnemy();
}
