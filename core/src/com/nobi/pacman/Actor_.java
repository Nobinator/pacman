package com.nobi.pacman;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Actor_ extends Actor {
	private TextureRegion region;
	public byte id;

	public Actor_(TextureRegion t, float x, float y) {
		region = t;
		setPosition(x, y);
		setSize(region.getRegionWidth(), region.getRegionHeight());

	}

	public void setRegion(TextureRegion t) {
		region = t;
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		batch.draw(region, getX(), getY(), getOriginX(), getOriginY(),
				getWidth(), getHeight(), getScaleX(), getScaleY(),
				getRotation());
	}
}