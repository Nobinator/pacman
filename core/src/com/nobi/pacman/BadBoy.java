package com.nobi.pacman;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class BadBoy extends Enemy {
    
    private float stateTime;
    
    public BadBoy(World w, TiledMap m, float x, float y){
        super(w, m, x, y);
        stateTime = 0f;
    }
    
    public void update(float dt){
        stateTime+=dt;
        setPosition(body.getPosition().x - getWidth() / 2, body.getPosition().x = getHeight() / 2);
        setBounds(getX(),getY(),12,16);
    }

    @Override
    void defineEnemy(){
        BodyDef bdef = new BodyDef();
        bdef.position.set(getX(),getY());
        bdef.type = BodyDef.BodyType.DynamicBody;
        body = world.createBody(bdef);
        body.setUserData(this);



        FixtureDef fdef = new FixtureDef();
        PolygonShape pg = new PolygonShape();
        //CircleShape cs = new CircleShape();

        //cs.setRadius(20f/Main.PPM);
        pg.setAsBox(12f, 16f,new Vector2(0,0),0);
        fdef.shape=pg;

        body.createFixture(fdef);

        pg.setAsBox(10f, 2f,new Vector2(0,-17f),0);
        fdef.shape = pg;
        fdef.isSensor = true;
        body.createFixture(fdef).setUserData("sensor");
    }
}
