package com.nobi.pacman;

import com.badlogic.gdx.physics.box2d.Fixture;

public class B2DUserdata{
    
    // Ключ - имя
    public String name;
    //Фикстура объекта, которому принадлежит этот экземпляр userdata
    public Fixture fixture;
    // Для чего нибудь
    public Object object;
    
    B2DUserdata(String name,Fixture fixture, Object obj){
        this.name = name;
        this.fixture = fixture;
        this.object = obj;
    }

    B2DUserdata(String name,Fixture fixture){
        this.name = name;
        this.fixture = fixture;
    }
}
