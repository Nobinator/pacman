package com.nobi.pacman;

public class log {
    //log logger = new log();

    public static void b(Object o){
        out(30, o);
    }

    public static void r(Object o){
        out(31, o);
    }

    public static void g(Object o){
        out(32, o);
    }

    public static void y(Object o){
        out(33, o);
    }

    public static void bl(Object o){
        out(34, o);
    }

    public static void m(Object o){
        out(35, o);
    }

    public static void c(Object o){
        out(36, o);
    }

    private static void out(int i, Object o){
        System.out.println("\u001b[" + i + "m" + o.toString() + "\u001b[39m");
    }
}
