package com.nobi.pacman;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

public class MenuScreen implements Screen {

	/** Main class copy () */
	Main Game;
	public boolean music;
	TextureRegion background;
	Texture t;
	ClickListener cl;
	public OrthographicCamera cam;
	/* buttons (b- prefix) */
	Actor_ bstart, label;

	SpriteBatch SB;

	Stage stage;
	public Group G1;

	public MenuScreen(SpriteBatch batch, Main game, OrthographicCamera camera) {
		Game = game;
		SB = batch;
		cam = camera;
		cam.zoom = 1f / Game.Scale;
		cam.position.x = 0;
		cam.position.y = 0;

		ScreenViewport v = new ScreenViewport();
		v.setCamera(cam);
		stage = new Stage(v, SB);

		G1 = new Group();
		G1.setVisible(true);

		bstart = new Actor_(Game.atlas.findRegion("bstart"),
				cam.position.x - 39, cam.position.y - 20);
		bstart.id = 1;
		label = new Actor_(Game.atlas.findRegion("label"), cam.position.x - 95,
				cam.position.y + 10);

		ClickListener cl = new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				out("click");

				switch (((Actor_) event.getListenerActor()).id) {

				/* bstart */case 1 :
						Game.showGame();
						break;
					/* bach ievs */// case 2 : Game.gi.getAchievementsGPGS();
									// break;
					/* blead */// case 4 :
								// Game.gi.getLeaderboardGPGS(GameInterface.endless_leaderboard);
								// break;
					default :
						out("unknown button "
								+ ((Actor_) event.getListenerActor()).id);
						break;
				}

			};
		};

		bstart.addListener(cl);

		G1.addActor(bstart);
		G1.addActor(label);
		stage.addActor(G1);

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		cam.update();

		/*
		 * SB.setProjectionMatrix(cam.combined); SB.begin(); SB.end();
		 */
		stage.act();
		stage.draw();
	}

	@Override
	public void resize(int w, int h) {

	}

	@Override
	public void show() {

		InputMultiplexer im = new InputMultiplexer();
		im.addProcessor(stage);
		Gdx.input.setInputProcessor(im);
		im = null;
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		stage.dispose();
	}

	public void out(Object s) {
		System.out.println(s.toString());
	}

}