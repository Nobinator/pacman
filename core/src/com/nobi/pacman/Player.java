package com.nobi.pacman;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.JointDef;
import com.badlogic.gdx.physics.box2d.MassData;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.AnimationStateData;
import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.SkeletonData;
import com.esotericsoftware.spine.SkeletonJson;
import com.esotericsoftware.spine.SkeletonRenderer;
import com.esotericsoftware.spine.SkeletonRendererDebug;


public class Player extends Sprite{

    //Spine
    Skeleton skeleton;
    AnimationState state;
    SkeletonRenderer renderer;
    SkeletonRendererDebug debugRenderer;
    
    Main main;


    public enum State {FALLING, JUMPING, STANDING, RUNNING}
    public State currentstate;
    public State previousstate;
    public float statetimer;
    public boolean canJump;
    
    public World world;
    public Body body;
    
    // Импульс передвижения в данный момент
    private float impulse = 0.5f; // 0.1 = 1 метр / 10 sec => 2 unit per 5 second
    
    private float max = 3f;
    
    // Импульс прыжка
    private float jimpulse = 1.5f;
    
    public Fixture p_left,p_right,p_body;
    
    public static float FRICTION_NORMAL = 0.99f;
    
    
    /* 
    
    * ОСНОВНЫЕ ЕДИНИЦЫ ИЗМЕРЕНИЯ :
    * Килограммы  -  m
    * Секунды     -  t
    * Метры       -  s
    
    * ФОРМУЛЫ :
    * [  p = u = метр / сек  ]
    * p = m*u
    * m = 1кг
    
    * frame = 1/60 сек
    *
    * [  u = метр / сек  ]
    *
    * impulse = метр / сек 
    * [0;1] 1
    * сек = метр / impulse
    *
    * с импульсом 0.1 мы пройдем 1 метр за 10 сек
    * 0.1 = 1 / сек
    * 
    * meter = 2 unit;
    * 
    */
    
    TextureRegion reg;
    
    float width,height;
    
    Player(World w, Main m,float posx,float posy){
        main = m;
        world = w;
        setPosition(posx,posy);
        currentstate = State.STANDING;
        previousstate = State.STANDING;
        statetimer = 0;
        canJump = true;

        
        
        
        
        
        //definePlayer(136f,168f);
        definePlayer(Main.unit*.5f,Main.unit*.9f);

        setupSpine();
    }
    
    public TextureRegion getFrame(float dt){
        currentstate = getState();
        
        TextureRegion region = null;
        
        switch(currentstate){
            case JUMPING: /* region =  animJump.getKeyFrame(dt)*/break;
        }
        /*Тернарный оператор :  условие                         true             false */
        statetimer =            currentstate == previousstate ? statetimer +dt : 0;
        
        return region;
    }
    
    public void update(float dt){
        
        currentstate = getState();
        previousstate = currentstate;

        state.update(dt);

        skeleton.setPosition(getBodyX(), getBodyY());

        state.apply(skeleton); // Poses skeleton using current animations. This sets the bones' local SRT.
        skeleton.updateWorldTransform(); // Uses the bones' local SRT to compute their world SRT.

        
        
        
        //log.c(body.getLinearVelocity().y);
    }
    
    
    public void draw(Batch sb, OrthographicCamera cam){
        //debugRenderer.getShapeRenderer().setProjectionMatrix(cam.combined);
        renderer.draw(sb, skeleton); // Draw the skeleton images.
        // Возвращаем прозрачность после рендра спайна, ибо сам он не возвращает исходные значения (ах он негодяй)
        sb.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        //debugRenderer.draw(skeleton); // Draw debug lines.
    }
    
    
    private void setupSpine(){
        renderer = new SkeletonRenderer();
        renderer.setPremultipliedAlpha(false); // PMA results in correct blending without outlines.

        debugRenderer = new SkeletonRendererDebug();
        debugRenderer.setBoundingBoxes(false);
        debugRenderer.setRegionAttachments(false);

        SkeletonJson json = new SkeletonJson(main.atlas); // This loads skeleton JSON data, which is stateless.
        json.setScale(0.002f); // Load the skeleton at 60% the size it was in Spine.
        SkeletonData skeletonData = json.readSkeletonData(Gdx.files.internal("skeleton.json"));
        
        skeleton = new Skeleton(skeletonData); // Skeleton holds skeleton state (bone positions, slot attachments, etc).
        
        AnimationStateData stateData = new AnimationStateData(skeletonData); // Defines mixing (crossfading) between animations. Определяет смешивания ( плавный ) между анимацией .
        //stateData.setMix("run", "jump", 0.2f);
        //stateData.setMix("jump", "run", 0.2f);

        state = new AnimationState(stateData); // Holds the animation state for a skeleton (current animation, time, etc).
        state.setTimeScale(0.1f); // Slow all animations down to 50% speed.

        // Queue animations on track 0.
        state.setAnimation(0, "walk", true);
        //state.addAnimation(0, "jump", false, 2); // Jump after 2 seconds.
        //state.addAnimation(0, "run", true, 0); // Run after the jump.
    }
    
    public State getState(){
        if(body.getLinearVelocity().y > 0){
            return State.JUMPING;
        }
        else if(body.getLinearVelocity().y < 0 ){
            return State.FALLING;
        }
        else if(body.getLinearVelocity().x != 0 ){
            return State.RUNNING;
        }
        else {
            return State.STANDING;
        }
    }
    
    public void definePlayer(float w, float h){
        
        float unit = Main.unit;
        width = w;
        height = h;
        
        //Описание тела
        BodyDef bdef = new BodyDef();
        bdef.position.set(getX(),getY());
        bdef.type = BodyDef.BodyType.DynamicBody;
        body = world.createBody(bdef);
        body.setUserData(this);
        
        B2DUserdata data;
        Fixture fixt;
        
        /*MassData massdata = new MassData();
        massdata.mass = 0.8f;
        body.setMassData(massdata);*/
        
        //body.setGravityScale(0.5f);
        
        
        // Описание парметров фикстуры
        FixtureDef fdef = new FixtureDef();
        
        // Физическая форма - полигон
        PolygonShape pg = new PolygonShape();
        // Физическая форма - круг
        CircleShape  cs = new CircleShape();


        //JointDef jdef = new JointDef();
        //jdef.type =JointDef.JointType.DistanceJoint;
        //http://www.iforce2d.net/b2dtut/joints-overview
        //http://www.iforce2d.net/b2dtut/joints-revolute
        
        // Circle radius
        float   r = w/2;
        Vector2 cir = new Vector2(0,0);
        float   pad = h*.2f;
        float   liney = (h - w)/2;
        float   linew = w*.25f  /* 25% от всей ширины */;
        float   lineh =(h - pad) /* 90% от всей высоты */;
        Vector2 line1 = new Vector2(r-(linew/2),liney);
        Vector2 line2 = new Vector2(-(r-(linew/2)),liney);
        float   sensorw = w*.5f;
        float   sensorh = w*.25f;
        Vector2 sensor =  new Vector2(0,-w/2 - sensorh);
        
        //=== BODY (FELLING)===
            fdef.filter.maskBits = Main.BIT_GROUND | Main.BIT_TRIGGER;
            fdef.filter.categoryBits = Main.BIT_BODY;
            // Фикстура тела персонажа
            //pg.setAsBox(w*0.8f/2, h/2,new Vector2(0,0),0);
            cs.setRadius(r);
            cs.setPosition(cir);
            fdef.shape=cs;
            fdef.isSensor = false;
            fdef.friction = FRICTION_NORMAL;

            p_body = body.createFixture(fdef);
            p_body.setUserData(new B2DUserdata("p_body", p_body));
            
            // Левый бок
            pg.setAsBox(linew/2, lineh/2,line1,0);
            fdef.shape=pg;
            fdef.isSensor = false;
            fdef.friction = 0f;
            p_left  = body.createFixture(fdef);
            p_left.setUserData(new B2DUserdata("p_left", p_left));
    
            // Правый бок
            pg.setAsBox(linew/2,lineh/2,line2,0);
            fdef.shape=pg;
            fdef.isSensor = false;
            fdef.friction = 0f;
            p_right  = body.createFixture(fdef);
            p_right.setUserData(new B2DUserdata("p_right", p_right));
        //===========

        
        //=== SERSORS ===
            fdef.filter.maskBits = Main.BIT_GROUND;
            fdef.filter.categoryBits = Main.BIT_SENSOR | Main.BIT_BODY;
            
            //Фикстура - сенсор - как-бы ноги персонажа
            pg.setAsBox(sensorw/2,sensorh/2,sensor,0);
            fdef.shape = pg;
            fdef.isSensor = true;
            fixt = body.createFixture(fdef);
            fixt.setUserData(new B2DUserdata("p_sensor", fixt, this));
        //===============
        
        
    }
    
    public void jump(){
        if (canJump){
            body.applyLinearImpulse(new Vector2(0, jimpulse), body.getWorldCenter(), true);
            //log.c(body.getMass()+"kg");
        }
    }

    public void moveRight(){
        if(body.getLinearVelocity().x<max)
        body.setLinearVelocity(body.getLinearVelocity().x+impulse, body.getLinearVelocity().y);
        
    }
    
    public void moveLeft(){
        if(body.getLinearVelocity().x>-max)
        body.setLinearVelocity(body.getLinearVelocity().x-impulse, body.getLinearVelocity().y);
    }

   
    
    public float getBodyX(){
        if(body!=null)return body.getPosition().x;
        return 0;
    }
    /** Bottom */
    public float getBodyY(){
        return body.getPosition().y - (width/2);
    }
    
    
    
}
