package com.nobi.pacman;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class B2DWorldCreator{
    
    B2DWorldCreator(Main MI,World W,TiledMap M){

        //create body and fixture variables
        BodyDef bdef = new BodyDef();
        PolygonShape shape = new PolygonShape();
        FixtureDef fdef = new FixtureDef();
        Body body;
        Fixture fixt;
        
        float k=Main.unit;
        
        /* Создаем осязаемый мир */
        
        bdef.type = BodyDef.BodyType.StaticBody;
        
        
        
        for(MapObject object : M.getLayers().get("Collision").getObjects().getByType(RectangleMapObject.class)){
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            // + половина т.к квадрат создается из середины тайла
            bdef.position.set(((rect.getX() + rect.getWidth() / 2) /Main.ppu* k), ((rect.getY() + rect.getHeight() / 2 )/Main.ppu*k));

            body = W.createBody(bdef);
            

            shape.setAsBox((rect.getWidth() / 2)/Main.ppu*k, (rect.getHeight() / 2)/Main.ppu*k);
            fdef.shape = shape;
            fdef.filter.categoryBits = Main.BIT_GROUND;
            
            
            fixt = body.createFixture(fdef);
            fixt.setUserData(new B2DUserdata("brick", fixt));
        }
        
        /* Создаем триггеры */

        fdef.isSensor = true;
        
        for(MapObject object : M.getLayers().get("Triggers").getObjects().getByType(RectangleMapObject.class)){
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            bdef.type = BodyDef.BodyType.StaticBody;
            // + половина т.к квадрат создается из середины тайла
            bdef.position.set(((rect.getX() + rect.getWidth() / 2) /Main.ppu* k), ((rect.getY() + rect.getHeight() / 2 )/Main.ppu*k));

            body = W.createBody(bdef);

            shape.setAsBox((rect.getWidth()/2)/Main.ppu*k, (rect.getHeight()/2)/Main.ppu*k);
            fdef.shape = shape;
            fdef.filter.categoryBits = Main.BIT_TRIGGER;
            
            fixt = body.createFixture(fdef);
            fixt.setUserData(new B2DUserdata("trigger"+object.getProperties().get("n"), fixt));
        }

        fdef.isSensor = false;
        
        
        
        W.setContactListener(new WorldContactListener(MI));
        
    }
    /*
    
    // platform
        BodyDef bdef = new BodyDef();
        bdef.position.set(10/PPM,10/PPM);
        bdef.type = BodyDef.BodyType.StaticBody;
        
        Body body = world.createBody(bdef);

        PolygonShape polyshape = new PolygonShape();
        polyshape.setAsBox(100/PPM, 5/PPM);

        FixtureDef fdef = new FixtureDef();
        fdef.shape = polyshape;
        
        fdef.filter.categoryBits = BIT_GROUND;
        fdef.filter.maskBits = BIT_CIRCLE;
        
        body.createFixture(fdef);
        //
        
        // ball
        bdef.position.set(20/PPM, 170/PPM);
        bdef.type =BodyDef.BodyType.DynamicBody;
        body = world.createBody(bdef);
                
        CircleShape cshape = new CircleShape();
        cshape.setRadius(5/PPM);
        fdef.shape = cshape;
        fdef.filter.categoryBits = BIT_CIRCLE;
        fdef.filter.maskBits = BIT_GROUND;
        body.createFixture(fdef);
    
    
    
    
     */
}
