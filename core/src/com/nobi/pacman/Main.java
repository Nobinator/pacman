package com.nobi.pacman;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Main extends Game {

	public TextureAtlas atlas;
	SpriteBatch SB;

	public byte Scale;
    //public static final float PPM = 140f;
    
    // Ширина, высота тайла и игрока
    public static final float unit = 0.5f;
    // Пикселей на метр
    public static final float ppm = 140/unit;
    // Пикселей на unit
    public static final float ppu = 140;
    
    // Приближение для камеры
    public static final float zoom = 0.01f;
    
    // биты (только степени двойки) для обозначения категории и маски
    // категория - тип объекта, по которому другие объекты проверяют, могут ли они с ними столкнутся
    // маска - типы объектов, по которым объект проверяет, может ли он столкнуться с другим объектом
    
    //
    // The important point is that these conditions must be satisfied for
    // both fixtures in order for collision to be allowed.
    //
    // Важным моментом является то, что эти условия должны быть выполнены
    // для обоеих фикстур для того, чтобы столкновение было разрешено.
    //
    
    public static final short BIT_GROUND        = 2;
    public static final short BIT_TRIGGER       = 4;
    public static final short BIT_BODY          = 8;
    public static final short BIT_SENSOR        = 16;

	public MenuScreen menu;
	public GameScreen game;

	public BitmapFont font;
    
    

	@Override
	public void create() {
		atlas = new TextureAtlas(Gdx.files.internal("atlas.atlas"));
		SB = new SpriteBatch();

		game = new GameScreen(SB, this);
		showGame();
	}
    
    /*public void putData(B2DUserdata data){
        B2DUserdatas.add(data);
        B2DUserdatas.
    }*/

	public String data;
	public char maze[][];

	public void showGame() {
		setScreen(game);
	}
	public void showMenu() {
        //TODO
		menu = new MenuScreen(SB, this, null);
		setScreen(menu);
	}

	public boolean isLoaded = false;

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}

	@Override
	public void dispose() {
		super.dispose();
	}

	private void out(Object o) {
		System.out.println(o.toString());
	}
}
