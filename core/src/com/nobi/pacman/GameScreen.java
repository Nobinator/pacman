package com.nobi.pacman;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.xguzm.pathfinding.gdxbridge.NavTmxMapLoader;
import org.xguzm.pathfinding.gdxbridge.NavigationTiledMapLayer;
import org.xguzm.pathfinding.grid.GridCell;
import org.xguzm.pathfinding.grid.NavigationGrid;
import org.xguzm.pathfinding.grid.finders.AStarGridFinder;
import org.xguzm.pathfinding.grid.finders.GridFinderOptions;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.AtlasTmxMapLoader;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.sun.prism.impl.shape.ShapeRasterizer;


public class GameScreen implements Screen{

    OrthographicCamera cam;
    private Viewport vp;
    SpriteBatch SB;
    Main main;
    Hud hud;

    World world;
    Box2DDebugRenderer b2dr;

    TiledMap tiledMap;
    TiledMapRenderer tiledMapRenderer;
    float tilesize=0;
    
    Player p;
    BadBoy bb;
    
	public GameScreen(SpriteBatch batch, Main arg2) {

		SB = batch;
		main = arg2;


        cam = new OrthographicCamera();
        vp = new FitViewport(Gdx.graphics.getWidth(),Gdx.graphics.getHeight(),cam);
        cam.position.set(vp.getWorldWidth()/2, vp.getWorldHeight()/2, 0);
        cam.zoom = Main.zoom;
        hud = new Hud(SB,this);
        world = new World(new Vector2(0,-10f), true);
        
        b2dr = new Box2DDebugRenderer();

        
        
        

		tiledMap = new TmxMapLoader().load("1.tmx");
        
        TiledMapTileLayer layer = (TiledMapTileLayer)tiledMap.getLayers().get(0);
        tilesize = layer.getTileWidth();
        
        tiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap, 1f / Main.ppm);
        
        new B2DWorldCreator(arg2,world,tiledMap);
        
        p = new Player(world,main,Main.unit*2,Main.unit*2);
        
        //bb = new BadBoy(world,tiledMap,3*48f+24f,48f+24f);

	}

    Vector3 pos;

    public void update(float dt){
        handleInput(dt);
        p.update(dt);
        //bb.update(dt);
        hud.update(dt);
        cam.position.x = p.body.getPosition().x;
        cam.position.y = p.body.getPosition().y;
        cam.update();
        tiledMapRenderer.setView(cam);
        world.step(1f/60f, 6, 2);
    }
    
    public void handleInput(float dt){
        if (Gdx.input.isKeyPressed(Input.Keys.UP)) p.jump();
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) p.moveRight();
        if(Gdx.input.isKeyPressed(Input.Keys.LEFT)) p.moveLeft();
    }

	@Override
	public void render(float delta) {

        Gdx.gl.glClearColor((float) 69/255, (float) 69/255, (float) 69/255, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        update(delta);

        SB.setProjectionMatrix(hud.stage.getCamera().combined);
        


        //tiledMapRenderer.render();

        b2dr.render(world,cam.combined);

        hud.stage.draw();
		
		SB.setProjectionMatrix(cam.combined);
		SB.begin();
        p.draw(SB,cam);
		SB.end();


		/*stage.act(delta);
		stage.draw();*/

	}
	Random r = new Random();

	@Override
	public void resize(int w, int h) {
		vp.update(w, h);
        log.r("vp update :"+w+", "+h);
	}
	@Override
	public void show(){
        InputMultiplexer im=new InputMultiplexer();
        im.addProcessor(new KeyboardListener());
		/*
		 * im.addProcessor(new SwipeListener(new Directions(){
		 * 
		 * @Override public void onLeft() { left(); }
		 * 
		 * @Override public void onRight() { right(); }
		 * 
		 * @Override public void onUp() { up(); }
		 * 
		 * @Override public void onDown() { down(); }})
		 * 
		 * );
		 */
		Gdx.input.setInputProcessor(im);
		im = null;
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
        tiledMap.dispose();
        world.dispose();
        b2dr.dispose();
        hud.dispose();
	}
    
    

	// ------------------------------------------------------------------

	/** KeyboardInput () */
        class KeyboardListener implements InputProcessor{

            @Override
            public boolean keyDown(int keycode){
                switch(keycode){
                    case Keys.LEFT:
                        break;
                    case Keys.RIGHT:
                        break;
                    case Keys.UP:
                        break;
                    case Keys.DOWN:
                        break;
                    case Keys.S:
                        cam.zoom-=0.5f;
                        log.b(cam.zoom);
                        break;
                    case Keys.W:
                        cam.zoom+=0.5f;
                        log.b(cam.zoom);
                        break;
                    case Keys.E:
                        cam.zoom+=0.1f;
                        log.b(cam.zoom);
                        break;
                    case Keys.D:
                        cam.zoom-=0.1f;
                        log.b(cam.zoom);
                        break;
                    case Keys.R:
                        cam.zoom+=0.001f;
                        log.b(cam.zoom);
                        break;
                    case Keys.F:
                        cam.zoom-=0.001f;
                        log.b(cam.zoom);
                        break;
                }

                return true;
            }

            @Override
            public boolean keyUp(int keycode){
                return false;
            }

            @Override
            public boolean keyTyped(char character){
                return false;
            }

            @Override
            public boolean touchDown(int x, int y, int pointer, int button){
                return false;
            }

            @Override
            public boolean touchUp(int screenX, int screenY, int pointer, int button){
                return false;
            }

            @Override
            public boolean touchDragged(int screenX, int screenY, int pointer){
                return false;
            }

            @Override
            public boolean mouseMoved(int screenX, int screenY){
                return false;
            }

            @Override
            public boolean scrolled(int amount){
                cam.zoom+=amount/1000f;
                return false;
            }
        }
    

	// Logging
	public void out(Object o) {
		System.out.println(o.toString());
	}

}
